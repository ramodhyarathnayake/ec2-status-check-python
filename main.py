import boto3
import schedule
import time

#client = boto3.client('ec2' , region_name = "eu-central-1")
ec2_client = boto3.client('ec2')
ec2_resources = boto3.resource('ec2')

reservations = ec2_client.describe_instances()

for reservation in reservations['Reservations']:
    instances =reservation['Instances']
    for instance in instances:
        print(f"Instance {instance['InstanceId']} is {instance['State']['Name']}")

def check_instance_status():
    statuses = ec2_client.describe_instance_status()
    for server_status in statuses ['InstanceStatuses']:
        instance_Status = server_status ['InstanceStatus']['Status']
        system_Status = server_status ['SystemStatus']['Status']
        state = server_status ['InstanceState']
        print(f"Instance {server_status['InstanceId']} is {state} with {instance_Status} and system status is {system_Status}")
    print("####################################\n")
schedule.every(5).seconds.do(check_instance_status)

while True:
    schedule.run_pending()
